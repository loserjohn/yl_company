/*
 * @Author       : xh
 * @Date         : 2022-05-16 09:37:48
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-17 11:36:28
 * @FilePath     : \company\src\api\app.js
 */
import request from '@/utils/request';

/**
 *
 *新闻列表
 * @export
 * @param {*} parmas
 * @return {*}
 */
export function news(params) {
  return request({
    url: '/news',
    method: 'get',
    params
  });
}

/**
 *
 *新闻详情
 * @export
 * @param {*} parmas
 * @return {*}
 */
export function newsDetail(id) {
  return request({
    url: `/news/${id}`,
    method: 'get'
  });
}

/**
 *
 *业务范畴
 * @export
 * @param {*} site
 * @return {*}
 */
export function product(site) {
  return request({
    url: '/product',
    method: 'get'
  });
}


/**
 *  关于游龙
 *
 * @export
 * @return {*}
 */
export function about() {
  return request({
    url: '/about',
    method: 'get'
  });
}


/**
 *
 *全局配置
 * @export
 * @return {*}
 */
export function config() {
  return request({
    url: '/config',
    method: 'get'
  });
}


/**
 *
 *联系我们
 * @export
 * @param {*} parmas
 * @return {*}
 */
export function contact(params) {
  return request({
    url: '/contact-us',
    method: 'get',
    params
  });
}


/**
 *
 *加入我们
 * @export
 * @param {*} parmas
 * @return {*}
 */
export function jobs(params) {
  return request({
    url: '/jobs',
    method: 'get',
    params
  });
}


/**
 *
 *职位详情
 * @export
 * @param {*} parmas
 * @return {*}
 */
export function jobsDetail(id) {
  return request({
    url: `/jobs/${id}`,
    method: 'get'
  });
}
