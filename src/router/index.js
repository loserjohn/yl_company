/*
 * @Author       : xh
 * @Date         : 2022-05-11 17:04:08
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-12 10:34:47
 * @FilePath     : \company\src\router\index.js
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/business',
    name: 'Business',
    component: ()=>import('../views/business/index.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    component: ()=>import('../views/contact/index.vue')
  },
  {
    path: '/join',
    name: 'Join',
    component: ()=>import('../views/join/index.vue')
  },
  {
    path: '/joinDetail/:id',
    name: 'JoinDetail',
    component: ()=>import('../views/joinDetail/index.vue')
  },
  {
    path: '/news',
    name: 'News',
    component: ()=>import('../views/news/index.vue')
  },
  {
    path: '/newsDetail/:id',
    name: 'NewsDetail',
    component: ()=>import('../views/newsDetail/index.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
