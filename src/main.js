/*
 * @Author       : xh
 * @Date         : 2022-05-11 17:01:03
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-17 16:29:20
 * @FilePath     : \company\src\main.js
 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';

// import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'hover.css';

Vue.config.productionTip = false;
Vue.use(ElementUI);
// Vue.use(VueBus)

Vue.prototype.$bus = new Vue();
if (window.innerWidth < 1026) {
  Vue.prototype.$ifMobile = true;
} else {
  Vue.prototype.$ifMobile = false;
}


new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app');
