/*
 * @Descripttion:
 * @version:
 * @ Modified by: loserjohn
 * @ Modified time: 2021-12-01 21:31:52
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-17 17:17:56
 */
// import store from '@/store'
// import {
//   getToken
// } from '@/utils/auth'
import axios from 'axios';
import { Loading, Message } from 'element-ui';
let loadingInstance = null;
let requstQueue = 0;
let reqList = []; // 防止重复请求
let cancel;


let baseKey = 'pc';
if (window.innerWidth < 1026) {
  baseKey = '1';
} else {
  baseKey = '0';
}


const options = {
  body: true,
  lock: true,
  text: 'Loading',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0)'
};


const stopRepeatRequest = function (reqList, config, cancel, errorMessage) {
  const {url, params} = config;
  const str = JSON.stringify(params);

  const uniName = `${url}_${str}`;
  // debugger;
  const errorMsg = errorMessage || '';
  for (let i = 0; i < reqList.length; i++) {
    if (reqList[i] === uniName) {
      console.warn(errorMessage);
      cancel(errorMsg);
      return;
    }
  }
  reqList.push(uniName);
  // allowRequest(reqList, url);
};

/**
 * 允许某个请求可以继续进行
 * @param {array} reqList 全部请求列表
 * @param {string} url 请求地址
 */
const allowRequest = function (reqList, config) {
  const {url, params} = config;
  const str = JSON.stringify(params);

  const uniName = `${url}_${str}`;
  for (let i = 0; i < reqList.length; i++) {
    if (reqList[i] === uniName) {
      reqList.splice(i, 1);
      break;
    }
  }
};
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    // do something before request is sent


    config.headers['is-m-site'] = baseKey;
    let cancel;
    // 设置cancelToken对象
    config.cancelToken = new axios.CancelToken(function(c) {
      cancel = c;
    });
    // 阻止重复请求。当上个请求未完成时，相同的请求不会进行
    stopRepeatRequest(reqList, config, cancel, `${config.url} 重复请求被中断`);

    // showLoading();
    return config;
  },
  (error) => {
    // do something with request error

    hideLoading();
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(

  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code.
   */
  (response) => {
    const res = response.data;
    // debugger;
    // hideLoading();
    allowRequest(reqList, response.config);
    if (response.status >= 200 && response.status < 300) {

      if (res.code === 200) {
        return res.data;
      } else {
        Message({
          message: JSON.stringify(res),
          type: 'error',
          duration: 5 * 1000
        });
        return Promise.reject(res);
      }

    } else {
      Message({
        message: JSON.stringify(res),
        type: 'error',
        duration: 5 * 1000
      });
      // return res
      return Promise.reject(res);
    }
    // if (res.status ===  401) {
    //   // to re-login
    //   MessageBox.confirm('身份过期，请重新登录。', '确认退出', {
    //     confirmButtonText: '重新登陆',
    //     cancelButtonText: '取消',
    //     type: 'warning'
    //   }).then(() => {
    //     store.dispatch('user/resetToken').then(() => {
    //       location.reload()
    //     })
    //   })
    // }else{

    // }
  },
  (error) => {
    hideLoading();

    // console.log('err：', error.response) // for debug
    const err = error.response.data || null;
    Message({
      message: err && err.message ? err.message : err.request_url,
      type: 'error',
      duration: 5 * 1000
    });
    // if (error.message.indexOf('401') >= 0) {
    //   router.push({
    //     path: '/401'
    //   })
    // }

    return Promise.reject(err);
  }
);


/**
 *全局loading
 *
 */
function showLoading() {

  if (requstQueue <= 0) {
    console.log('loading', requstQueue);
    loadingInstance = Loading.service(options);
    requstQueue = 0;
  }
  requstQueue += 1;
}


/**
 *关闭全局loading
 *
 */
function hideLoading() {
  requstQueue -= 1;
  if (requstQueue <= 0) {
    console.log('hideLoading', requstQueue);
    loadingInstance.close();
    loadingInstance = null;
    requstQueue = 0;
  }

}

export default service;
