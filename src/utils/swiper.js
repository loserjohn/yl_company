/*
 * @Author       : xh
 * @Date         : 2022-05-12 15:19:57
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-12 17:47:33
 * @FilePath     : \company\src\utils\swiper.js
 */

// ele 轮播图 添加 手势滑动

/** name @type {*}   dom选择器 */
/** refdom @type {*} ref节点 */
export const swiper = (name, refdom, cb) => {


  let box = document.querySelector(name);
  if (!box) {
    throw new Error('找不到改元素');
    // return
  }
  let startPoint = 0;
  let stopPoint = 0;
  // 重置坐标
  let resetPoint = function () {
    startPoint = 0;
    stopPoint = 0;
  };
  // 手指按下
  box.addEventListener('touchstart', function (e) {
    // 手指点击位置的X坐标
    startPoint = e.changedTouches[0].pageX;
  });
  // 手指滑动
  box.addEventListener('touchmove', function (e) {
    // 手指滑动后终点位置X的坐标
    stopPoint = e.changedTouches[0].pageX;
  });
  // 当手指抬起的时候，判断图片滚动离左右的距离

  box.addEventListener('touchend', function () {
    if (stopPoint == 0 || startPoint - stopPoint == 0) {
      resetPoint();
      return;
    }
    if (startPoint - stopPoint > 0) {
      resetPoint();
      if (cb) {
        cb('next');
        return;
      }

      refdom.next();

      return;
    }
    if (startPoint - stopPoint < 0) {
      resetPoint();
      if (cb) {
        cb('prev');
        return;
      }
      refdom.prev();

      return;
    }
  });
};
